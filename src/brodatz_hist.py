import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from PIL import Image

def pixel_dist(in_dir, out_dir, ext):
    for i in range(1,113,4):
        fig = plt.figure()
        plt.ylabel('Density')
        plt.xlabel('Grayscale')
        plt.title('Graylevel density pixel distribution')

        for j in range(i,i+4):
            im = Image.open(in_dir + 'D' + str(j) + ext, mode='r')
            im = np.array(im)
            sns.distplot(im.ravel(), hist=False, kde=True, kde_kws={'linewidth': 2}, label='D'+str(j))

        plt.legend()
        plt.savefig(out_dir + str(i))
        plt.close()


if (__name__=='__main__'):
    in_dir_norm = '../data/brodatz_normalized/'
    ext_norm = '.tif'
    out_dir_norm = '../data/results/brodatz_norm_dist/'

    in_dir = '../data/brodatz/'
    ext = '.png'
    out_dir = '../data/results/brodatz_dist/'

    pixel_dist(in_dir, out_dir, ext)
    pixel_dist(in_dir_norm, out_dir_norm, ext_norm)


