import sys
import os
import midpoint as mp

def print_help():
    print('Usage:')
    print('python genSamples.py <recursion level> <fractal dimension> <number of samples>')
    print('<number of samples> is optional, default values is 1')

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print('ERROR: Not enough arguments')
        print_help()
        exit()
    elif len(sys.argv) > 4:
        print('ERROR: To many arguments')
        print_help()
        exit()

    level = int(sys.argv[1])
    fractal_dim = float(sys.argv[2])
    num_samples = 1

    if len(sys.argv) == 4:
        num_samples = int(sys.argv[3])

    print('Generating ' + str(num_samples) + ' samples')
    print('Recursion level: ' + str(level))
    print('Theoretical fractal dimension: ' + str(fractal_dim))

    path = '../data/samples_' + str(fractal_dim)
    os.mkdir(path)
    os.chdir(path)

    i = 0
    while i < num_samples:
        print('Sample: (' + str(i + 1) + '/'+str(num_samples)+')')
        sample = mp.Fractal_surface(level, 1, 3.0 - fractal_dim)

        sample.save_img(str(i))
        i += 1
