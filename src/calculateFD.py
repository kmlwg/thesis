import numpy as np
import cv2
import scipy.stats as stats
import pandas as pd
import matplotlib.pyplot as plt
import sys
import os
from PIL import Image

import boxCounting as bc


def read(filename):
    img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)

    if type(img) == None:
        print("File does not exist or unhandled format")
        return
    else:
        img = np.array(img)
        return img


def calc_df_bc(FD, resolution, n):
    """
    Calculate parameters for a given number od samples.
    Returns numpy array.
    FD - Fractal dimension of given samples
    n  - number of samples samples
    """
    results = np.empty([n, 4])
    for i in np.arange(0, n):
        img = read('../data/' + str(resolution) + '/samples_' +
                   str(FD) + '/' + str(i) + '.png')
        img_res = bc.df_bc(img, resolution, resolution//2)

        results[i][0] = img_res['slope']
        results[i][1] = img_res['corelation']
        results[i][2] = img_res['std_err']
        results[i][3] = img_res['fit_err']
        print('(' + str(i+1) + '/' + str(n) + ')')

    return results


def calc_imdf_bc(FD, resolution, n):
    """
    Calculate parameters for a given number od samples.
    Returns numpy array.
    FD - Fractal dimension of given samples
    n  - number of samples samples
    """
    results = np.empty([n, 4])
    for i in np.arange(0, n):
        img = read('../data/' + str(resolution) + '/samples_' +
                   str(FD) + '/' + str(i) + '.png')
        img_res = bc.imdf_bc(img, resolution, resolution//2)

        results[i][0] = img_res['slope']
        results[i][1] = img_res['corelation']
        results[i][2] = img_res['std_err']
        results[i][3] = img_res['fit_err']
        print('(' + str(i+1) + '/' + str(n) + ')')

    return results


def to_data_frame(results, n):
    """
    Converts numpy array to pandas dataframe.
    results - array with results
    n - number of samples
    """
    data = pd.DataFrame(
        {
            'slope': results[0:n, 0],
            'corelation': results[0:n, 1],
            'std_error': results[0:n, 2],
            'fit_error': results[0:n, 3]
        }, index=np.arange(0, n)
    )
    return data


def gen_tally(method, res):
    """
    Generates a tally for given method and resolution of set of
    Brownian motion surfaces. Uses files in /data/results/<resolution>/

    method - string value for method - 'df_bc' or 'imdf_bc'
    res - int value for resoultion - 257, 513, 1025
    """
    ext = '.csv'
    avg_fd = []
    avg_fit_error = []
    std_dev = []

    for i in range(10):
        fd = 2.0 + i * 0.1
        in_file = method + '_' + str(fd) + ext
        sheet = pd.read_csv('../data/results/' + str(res) + '/' + in_file)
        avg_fd.append(np.average(sheet['slope']))
        avg_fit_error.append(np.average(sheet['fit_error']))
        std_dev.append(np.std(sheet['slope']))

    tally = pd.DataFrame({
        'computional_FD': avg_fd,
        'fit_error': avg_fit_error,
        'theoretical_FD': np.arange(2.0, 3.0, 0.1),
        'std_deviation': std_dev
    })

    tally.to_csv('../data/results/' + method + '_' + str(res) + '.csv')


def brodatz():
    """
    Calculates results for Brodatz textures
    """
    name = '../data/brodatz/D'
    ext = '.png'
    n_img = 113

    fit_error = []
    fd = []

    for i in range(1, n_img):
        print(str(i) + "/112")
        img = read(name + str(i) + ext)
        res = bc.imdf_bc(img, 640, 320)
        fd.append(res['slope'])
        fit_error.append(res['fit_err'])

    data = pd.DataFrame({
        'FD': fd,
        'err': fit_error
    })
    data.index += 1

    data.to_csv('../data/results/brodatz_imdf_bc.csv')


def brodatz_norm():
    """
    Calculates results for normalized Brodatz textures
    """
    src = '../data/brodatz_normalized/D'
    ext = '.tif'
    n_img = 113

    fit_error = []
    fd = []

    for i in range(1,n_img):
        print(str(i) + "/112")
        img = Image.open(src + str(i) + ext, mode='r')
        img = np.array(img, dtype=np.uint8)
        res = bc.imdf_bc(img, 640, 320)
        fd.append(res['slope'])
        fit_error.append(res['fit_err'])

    data = pd.DataFrame({
        'FD': fd,
        'err': fit_error
    })
    data.index += 1

    data.to_csv('../data/results/brodatz_normalized_imdf_bc.csv')

    fit_error = []
    fd = []

    for i in range(1,n_img):
        print(str(i) + "/112")
        img = Image.open(src + str(i) + ext, mode='r')
        img = np.array(img, dtype=np.uint8)
        res = bc.df_bc(img, 640, 320)
        fd.append(res['slope'])
        fit_error.append(res['fit_err'])

    data = pd.DataFrame({
        'FD': fd,
        'err': fit_error
    })
    data.index += 1

    data.to_csv('../data/results/brodatz_normalized_df_bc.csv')


def compare_res(method):
    """
    Compares results for the given methof for different resultions.
    method - string value for methof - 'df_bc' or 'imdf_bc'
    """
    tally = pd.read_csv('../data/results/' + method + '_257.csv')

    fig = plt.figure()
    plt.ylabel('Computational FD')
    plt.xlabel('Theoretical FD')

    plt.plot(
        tally['theoretical_FD'],
        tally['computional_FD'],
        'b',
        label='257x257'
    )

    plt.errorbar(
        tally['theoretical_FD'],
        tally['computional_FD'],
        yerr=tally['std_deviation'],
        fmt='b',
        label='257x257 std deviation'
    )

    tally = pd.read_csv('../data/results/' + method + '_513.csv')

    plt.plot(
        tally['theoretical_FD'],
        tally['computional_FD'],
        'r',
        label='513x513'
    )

    plt.errorbar(
        tally['theoretical_FD'],
        tally['computional_FD'],
        yerr=tally['std_deviation'],
        fmt='r',
        label='513x513 std deviation' 
    )

    tally = pd.read_csv('../data/results/' + method + '_1025.csv')

    plt.plot(
        tally['theoretical_FD'],
        tally['computional_FD'],
        'g',
        label='1025x1025'
    )

    plt.errorbar(
        tally['theoretical_FD'],
        tally['computional_FD'],
        yerr=tally['std_deviation'],
        fmt='g',
        label='1025x1025 std deviation'
    )

    plt.show()
    plt.legend()
    fig.savefig('../data/results/' + method + 'res_comp.png')



if __name__ == "__main__":
    # # Calculate results for Brownian surfaces
    # file_name = "df_bc_"
    # ext = ".csv"

    # avg_fit_err = []
    # avg_fd = []

    # for i in np.arange(0, 10):
    #     fd = 2.0 + i * 0.1
    #     print('Calculating samples of FD: ' + str(fd))
    #     res = calc_df_bc(fd, 513, 100)
    #     res = to_data_frame(res, 100)
    #     res.to_csv('../data/results/513/' + file_name +
    #                str(fd) + ext, index=False)
    #     avg_fd.append(np.average(res['slope']))
    #     avg_fit_err.append(np.average(res['fit_error']))


    # avg_fit_err = []
    # avg_fd = []
    # file_name = "imdf_bc_"

    # for i in np.arange(0, 10):
    #     fd = 2.0 + i * 0.1
    #     print('Calculating samples of FD: ' + str(fd))
    #     res = calc_imdf_bc(fd, 513, 100)
    #     res = to_data_frame(res, 100)
    #     res.to_csv('../data/results/513/' + file_name +
    #                str(fd) + ext, index=False)
    #     avg_fd.append(np.average(res['slope']))
    #     avg_fit_err.append(np.average(res['fit_error']))

    # gen_tally('df_bc', 513)
    # gen_tally('imdf_bc', 513)

    # Compare basic box counting method for different resolutions
    compare_res('df_bc')
    compare_res('imdf_bc')

    # # Plot comparison of both methods for Brownian motion surfaces
    # # for 1025 resolution
    # input = 'df_bc_1025.csv'
    # tally = pd.read_csv('../data/results/' + input)

    # fig = plt.figure()
    # plt.ylabel('Computational FD')
    # plt.xlabel('Theorethical FD')

    # bc = plt.plot(
    #     tally['theoretical_FD'],
    #     tally['computional_FD'],
    #     'b',
    #     label='BC'
    # )

    # bc_deviation = plt.errorbar(
    #     tally['theoretical_FD'],
    #     tally['computional_FD'],
    #     yerr=tally['std_deviation'],
    #     fmt='b',
    #     label='BC stdandard deviation'
    # )

    # input = 'imdf_bc_1025.csv'
    # tally = pd.read_csv('../data/results/' + input)

    # imbc = plt.plot(
    #     tally['theoretical_FD'],
    #     tally['computional_FD'],
    #     'r',
    #     label='IMBC'
    # )

    # imbc_deviation = plt.errorbar(
    #     tally['theoretical_FD'],
    #     tally['computional_FD'],
    #     yerr=tally['std_deviation'],
    #     fmt='r',
    #     label='IMBC standard deviation'
    # )
    # plt.legend()
    # plt.show()
    # fig.savefig('../data/results/borwnian_comp.png')

    # # Calculate results for Brodatz sets
    # brodatz()
    # brodatz_norm()