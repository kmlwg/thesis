import numpy as np
import cv2
import matplotlib.pyplot as plt
import scipy.stats as stats


def read(filename):
    img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)

    if type(img) is None:
        print("File does not exist or unhalded format")
        return
    else:
        img = np.array(img)
        return img


def get_block_bounds(x, y, i):
    """
    Calculates upper and lower bounds of the block
    ============================================================================
    Arguments
    ----------------------------------------------------------------------------
    x, y [int] - current position in the picture plane
    i    [int] - current block size

    Returns
    ----------------------------------------------------------------------------
    x_l, x_u, y_l, y_u  [int] - x, y upper and lower bounds
    """
    x_l = x * i
    x_u = (x+1) * i - 1
    y_l = y * i
    y_u = (y+1) * i - 1

    return x_l, x_u, y_l, y_u


def get_fit_error(r, N, slope, intercept):
    """
    Calculates the fit error
    ============================================================================
    Arguments
    ----------------------------------------------------------------------------
    r [list]

    Returns
    ----------------------------------------------------------------------------
    E [float] - fit error
    """
    E = 0.
    
    for i in np.arange(0, len(r)):
        temp = (slope * r[i] + intercept - N[i]) ** 2
        temp /= 1 + slope * slope
        E += temp

    E = np.divide(E, len(r))

    return E

def df_bc(img, M, s):
    """
    Differential Box Counting method
    [1] "A practical method for estimating fractal dimension" - X.C. Jin,
        S.H. Ong, Jayasooriah [Pattern Recognition Letters 16 (1995) 457-464]
    ============================================================================
    Arguments
    ----------------------------------------------------------------------------
    img - image of which FD is to be determinedS
    M - size of the side of the plane
    s - size of box

    Returns
    ----------------------------------------------------------------------------
    dict {'slope': slope, 'intercept': intercept, 'corelation': r_value,
          'std_err': std_err, 'log(N)': N, 'log(1/r)': r, 'fit_err': E}
    """
    if s > M/2 or s <= 1:
        print('Error: Incorect size of the box')
        return

    N = []
    r = []
    G = np.max(img) - np.min(img)
    i = 2
    while i <= s:
        m = M//i
        # Calculate the height of boxes
        h_box = G // (M//i)
        if h_box != 0:
            n = []

            for x in np.arange(0, m):
                for y in np.arange(0, m):
                    x_l, x_u, y_l, y_u = get_block_bounds(x, y, i)

                    z_max = np.max(img[x_l: x_u, y_l: y_u])
                    z_min = np.min(img[x_l: x_u, y_l: y_u])

                    l = np.ceil(np.divide(z_max, h_box))
                    k = np.ceil(np.divide(z_min, h_box))

                    n.append(l - k + 1)

            r.append(np.divide(i, M))
            N.append(np.sum(n))

        i = i * 2

    r = np.log(np.divide(1, r))
    N = np.log(N)

    slope, intercept, r_value, p_value, std_err = stats.linregress(
        r[1:-1], N[1:-1])

    # Calculate fit error
    E = get_fit_error(r, N, slope, intercept)

    return {'slope': slope, 'intercept': intercept, 'corelation': r_value,
            'std_err': std_err, 'log(N)': N, 'log(1/r)': r, 'fit_err': E}


def _shift_blocks(x, y, sigma, M):
    if x < M and y < M:
        return x + sigma, y + sigma
    elif x < M and y == M:
        return x + sigma, y - sigma
    elif x == M and y < M:
        return x - sigma, y + sigma
    elif x == M and y == M:
        return x - sigma, y - sigma


def imdf_bc(img, M, s):
    """
    An improved diferential box counting algorithm
    [1] "An improved differential box-counting method to estimate fractaldimensions of gray-level images" -
          - Yu Liu, Lingyu Chen, Heming Wang, Lanlan Jiang, Yi Zhang, Jiafei Zhao, Dayong Wang, 
            Yuechao Zhao,Yongchen Song [J. Vis. Commun. Image
         2014) 1102–1111]   https://www.sciencedirect.com/science/article/pii/S1047320314000649
    ====================================================================
    img - image of which FD is to be determined
    M - size of the side of the plane
    s - ---------------------------------size of box

    Returns
    --------------------------------------------------------------------
    dict {'slope': slope, 'intercept': intercept, 'corelation': r_value,
          'std_err': std_err, 'log(N)': N, 'log(1/r)': r, 'fit_err': E}
    --------------------------------"""
    if s > M/2 or s <= 1:
        print('Error: Incorect size of the box')
        return

    N = []
    r = []
    G = np.max(img) - np.min(img)
    sigma = 1
    i = 2
    while i <= s:
        m = M//i
        # Calculate the height of boxes
        h_box = G // (M//i)
        if h_box != 0:
            n = []

            for x in np.arange(0, m):
                for y in np.arange(0, m):
                    x_l, x_u, y_l, y_u = get_block_bounds(x, y, i)

                    z_max = np.max(img[x_l: x_u, y_l: y_u])
                    z_min = np.min(img[x_l: x_u, y_l: y_u])

                    l = np.ceil(np.divide(z_max, h_box))
                    k = np.ceil(np.divide(z_min, h_box))

                    n_old = l - k + 1.0/h_box

                    x_s, y_s = _shift_blocks(x, y, sigma, m - sigma)
                    x_l, x_u, y_l, y_u = get_block_bounds(x_s, y_s, i)


                    z_max = np.max(img[x_l: x_u, y_l: y_u])
                    z_min = np.min(img[x_l: x_u, y_l: y_u])

                    l = np.floor(np.divide(z_max, h_box))
                    k = np.floor(np.divide(z_min, h_box))

                    n_new = l - k + 1.0/h_box

                    n.append(n_old if n_old > n_new else n_new)

            r.append(np.divide(i, M))
            N.append(np.sum(n))

        i = i * 2

    r = np.log(np.divide(1, r))
    N = np.log(N)

    slope, intercept, r_value, p_value, std_err = stats.linregress(
        r[1:-1], N[1:-1])

    # Calculate fit error
    E = get_fit_error(r, N, slope, intercept)

    return {'slope': slope, 'intercept': intercept, 'corelation': r_value,
            'std_err': std_err, 'log(N)': N, 'log(1/r)': r, 'fit_err': E}


if __name__ == "__main__":
    surface = read("../data/brodatz/D8.png")

    # plt.imshow(surface)
    # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
    # plt.show()

    results = df_bc(surface, 640, 320)
    print("slope: " + str(results['slope']))
    results = imdf_bc(surface, 640, 320)

    print("slope: " + str(results['slope']))
    # print("intercept: " + str(c))
    # print("corelation: " + str(r_value))
    # print("stderr: " + str(std_err))
    # print("fit error: " + str(E))
    # # print(r)

    fig = plt.figure()
    plt.ylabel('log(1/r)')
    plt.xlabel('log(N)')
    plt.plot(results['log(1/r)'], results['log(N)'], 'o')

    # Plot fitted line
    plt.plot(results['log(1/r)'], results['slope'] *
             results['log(1/r)'] + results['intercept'], 'r')
    plt.show()
    plt.pause(10)
    fig.savefig('l.png')
