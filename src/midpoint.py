import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D


class Fractal_surface:
    """
    maxlevel [int] - maximal number of recursions
    sigma [float] - initial standard deviation
    alpha [float] - const value, determines roughness
    random_addition [bool] - if values is true, in each step points 
                             are randomly displaced
    """

    def __init__(self, maxlevel, sigma, alpha, random_addition=False):
        self.N = np.power(2, maxlevel)
        self.maxlevel = maxlevel
        self.sigma = sigma
        self.alpha = alpha
        self.surface = np.zeros([(self.N+1), (self.N+1)], dtype=np.float64)
        self._mid_point()
        self.random_addition = random_addition

    def _mid_point(self):
        # Set initial random values for the corners
        delta = self.sigma
        self.surface[0][0] = np.random.normal(scale=delta)
        self.surface[0][self.N] = np.random.normal(scale=delta)
        self.surface[self.N][0] = np.random.normal(scale=delta)
        self.surface[self.N][self.N] = np.random.normal(scale=delta)

        D = self.N
        d = self.N // 2

        for _ in np.arange(0, self.maxlevel):
            # Going from grid type 1 to 2 (square to diamond)

            delta *= np.power(0.5, 0.5 * self.alpha)

            # interpolate offset points
            for x in np.arange(d, self.N-d+1, D, dtype=np.int32):
                for y in np.arange(d, self.N-d+1, D, dtype=np.int32):
                    self.surface[x][y] = self._f4(delta, self.surface[x + d][y + d],
                                                  self.surface[x + d][y - d],
                                                  self.surface[x - d][y + d],
                                                  self.surface[x - d][y - d])

            # displace other points if needeed
            if self.random_addition:
                self._displace(delta, D)

            # going from grid type 2 to 1 (diamond to square)
            delta *= np.power(0.5, 0.5 * self.alpha)

            # interpolate and offset boudary grid points
            for x in np.arange(d, self.N-d+1, D, dtype=np.int32):
                self.surface[x][0] = self._f3(delta, self.surface[x+d][0],
                                              self.surface[x-d][0],
                                              self.surface[x][d])

                self.surface[x][self.N] = self._f3(delta, self.surface[x+d][self.N],
                                                   self.surface[x-d][self.N],
                                                   self.surface[x][self.N-d])

                self.surface[0][x] = self._f3(delta, self.surface[0][x+d],
                                              self.surface[0][x-d],
                                              self.surface[d][x])

                self.surface[self.N][x] = self._f3(delta, self.surface[self.N][x+d],
                                                   self.surface[self.N][x-d],
                                                   self.surface[self.N-d][x])

            # interpolate and offset interior grid points
            for x in np.arange(d, self.N-d+1, D, dtype=np.int32):
                for y in np.arange(D, self.N-d+1, D, dtype=np.int32):
                    self.surface[x][y] = self._f4(delta, self.surface[x][y+d],
                                                  self.surface[x][y-d],
                                                  self.surface[x+d][y],
                                                  self.surface[x-d][y])

            for x in np.arange(D, self.N-d+1, D, dtype=np.int32):
                for y in np.arange(d, self.N-d+1, D, dtype=np.int32):
                    self.surface[x][y] = self._f4(delta, self.surface[x][y+d],
                                                  self.surface[x][y-d],
                                                  self.surface[x+d][y],
                                                  self.surface[x-d][y])

            # displace other points also if needed
            if self.random_addition:
                self._displace(delta, D)

                # displace points for the 2nd type grid
                for x in np.arange(0, self.N-d, D):
                    for y in np.arange(0, self.N-d, D):
                        self.surface[x][y] += np.random.normal(scale=delta)

            D //= 2
            d //= 2

    def _f3(self, delta, x0, x1, x2):
        return (x0 + x1 + x2) / 3.0 + np.random.normal(scale=delta)

    def _f4(self, delta, x0, x1, x2, x3):
        return (x0 + x1 + x2 + x3) / 4.0 + np.random.normal(scale=delta)

    def _displace(self, delta, D):
        for x in np.arange(0, self.N+1, D):
            for y in np.arange(0, self.N+1, D):
                self.surface[x][y] += np.random.normal(scale=delta)

    def submerge(self):
        """
        All values below zero, become zero.
        """
        for x in np.arange(0, self.N+1):
            for y in np.arange(0, self.N+1):
                if self.surface[x][y] < 0:
                    self.surface[x][y] = 0

    def save_img(self, name):
        """
        Saves a fractal brownian surface (numpy array) to a png image.
        name [string] - name of file
        """
        img = self.surface
        min_val = np.min(img)

        if min_val < 0:
            min_val = np.abs(min_val)
            img = np.add(img, min_val)
            
        max_val = np.max(img)

        img = np.floor(np.multiply(np.divide(img, max_val), 255.0))

        cv2.imwrite(name + ".png", img, [cv2.IMWRITE_PNG_COMPRESSION, 0])

    def plot(self):
        """
        Plots the surface in 3D
        """
        fig = plt.figure()
        ax = plt.axes(projection='3d')

        x = np.linspace(0, self.N + 1, self.N + 1)
        y = np.linspace(0, self.N + 1, self.N+1)
        X, Y = np.meshgrid(x, y)
        ax.plot_surface(X, Y, self.surface, cmap=cm.coolwarm)

        for angle in range(0, 360):
            ax.view_init(30, angle)
            plt.draw()
            plt.pause(.1)


if __name__ == "__main__":
    surf = Fractal_surface(9, 1.0, 1.0)
    # surf.submerge()
    surf.save_img("Brown")
    surf.plot()
